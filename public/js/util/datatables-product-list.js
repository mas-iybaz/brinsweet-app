// Datatable
new DataTable('#dataProduk', {
    language: {
        url: '//cdn.datatables.net/plug-ins/1.13.6/i18n/id.json',
    },
    processing: true,
    serverSide: true,
    responsive: true,
    lengthChange: true,
    autoWidth: false,
    // ajax: "{{ route('product.index') }}",
    ajax: dt_product_list_route,
    columns: [{
        data: "DT_RowIndex",
        orderable: false,
        searchable: false
    }, {
        data: "name"
    }, {
        data: "category"
    }, {
        data: "qty"
    }, {
        data: "price"
    }]
});