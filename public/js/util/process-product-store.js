function confirmData(e) {
    e.preventDefault();

    Swal.fire({
        title: 'Anda Yakin?',
        text: "Data produk akan diunggah di penyimpanan!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Unggah Data',
        cancelButtonText: 'Batalkan'
    }).then((result) => {
        if (result.isConfirmed) {
            let form = document.getElementById('productForm');
            let formData = new FormData(form);

            $.ajax({
                // url: "{{ route('product.store') }}",
                url: dt_product_store_route,
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function (response) {
                    Swal.fire(
                        'Berhasil!',
                        'Data produk berhasil diunggah',
                        'success'
                    );        
                },
                error: function (xhr, status, error) {
                    let res = xhr.responseJSON;

                    Swal.fire(
                        res.message,
                        res.data,
                        'error'
                    );        
                }
            });

            document.getElementById('productForm').reset();
        }
    })
}