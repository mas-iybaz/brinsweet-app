// Datatable
new DataTable('#dataUser', {
    language: {
        url: '//cdn.datatables.net/plug-ins/1.13.6/i18n/id.json',
    },
    processing: true,
    serverSide: true,
    responsive: true,
    lengthChange: true,
    autoWidth: false,
    // ajax: "{{ route('user') }}",
    ajax: dt_user_list_route,
    columns: [{
        data: "DT_RowIndex",
        orderable: false,
        searchable: false
    }, {
        data: "name"
    }, {
        data: "email"
    }, {
        data: "created_at"
    }, {
        data: "updated_at"
    }]
});