@extends('layouts.app')

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Produk</h1>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4>Unggah Produk</h4>
                    </div>
                    <div class="card-body">
                        <form id="productForm" action="{{ route('product.store') }}" method="post" enctype="multipart/form-data" onsubmit="confirmData(event)">
                            @csrf
                            <input type="hidden" name="user_id" value="4">
                            <input type="hidden" name="user_name" value="Yasmin Haag">
                            <div class="form-group">
                                <label>File (*.csv)</label>
                                <div class="custom-file">
                                    <input type="file" name="file" class="custom-file-input">
                                    <label class="custom-file-label">Choose File</label>
                                </div>
                            </div>
                            <div>
                                <button class="btn btn-primary" type="submit">Unggah</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card card-primary">
                    <div class="card-header">
                        <h4>Format unggah file</h4>
                    </div>
                    <div class="card-body">
                        <ul>
                            <li>Format file yang diunggah adalah <span class="font-weight-bold">.csv</span></li>
                            <li>Format header data pada file adalah: <span class="font-weight-bold">name,category,qty,price</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('custom-js')
<script>
    let dt_product_store_route = "{{ route('product.store') }}"
</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script src="{{ asset('js/util/process-product-store.js') }}"></script>
@endpush