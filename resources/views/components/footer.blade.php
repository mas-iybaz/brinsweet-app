<footer class="main-footer">
    <div class="footer-left">
      Copyright &copy; 2023 <div class="bullet"></div> By <a href="https://mas-iybaz.github.io/" target="_blank">Muhammad Iqbal Aulia</a>
    </div>
    <div class="footer-right">
      <span class="font-weight-bold">BRINSWEET</span>
    </div>
</footer>