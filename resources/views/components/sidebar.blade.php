<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="{{ route('dashboard') }}">Brinsweet</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="{{ route('dashboard') }}">BS</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li class="{{ Request::is('dashboard') ? 'active' : '' }}"><a class="nav-link" href="{{ route('dashboard') }}">
                <i class="fas fa-fire"></i> <span>Dashboard</span></a>
            </li>
            
            <li class="menu-header">Main Menu</li>
            <li class="{{ Request::is('user') ? 'active' : '' }}"><a class="nav-link" href="{{ route('user') }}">
                <i class="fas fa-user"></i> <span>List User</span></a>
            </li>
            <li class="dropdown {{ Request::is('product*') ? 'active' : '' }}">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i
                        class="fas fa-columns"></i> <span>Produk</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ route('product.index') }}">List Produk</a></li>
                    <li><a class="nav-link" href="{{ route('product.create') }}">Upload Produk</a></li>
                </ul>
            </li>
    </aside>
</div>