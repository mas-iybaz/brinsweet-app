<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        $client = new Client(['base_uri' => env('SERVICE_HOST')]);

        $options = [
            'Accept' => 'application/json', 
        ];

        $req_data_statistic = $client->request(
            'GET',
            '/data-statistic',
            $options
        );
        $res_data_statistic = $req_data_statistic->getBody();

        $data_statistic = json_decode($res_data_statistic);
        $data_statistic = $data_statistic->data;

        return view('dashboard', compact('data_statistic'));
    }
}
