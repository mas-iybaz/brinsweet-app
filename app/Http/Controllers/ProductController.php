<?php

namespace App\Http\Controllers;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $client = new Client(['base_uri' => env('SERVICE_HOST')]);

        $options = [
            'Accept' => 'application/json', 
        ];

        // $req = $client->get('/product', $options);
        $req = $client->request(
            'GET',
            '/product',
            $options
        );
        $res = $req->getBody();

        $products = json_decode($res);
        $products = $products->data;

        if ($request->ajax()) {
            return datatables()->of($products)
            ->addIndexColumn()
            ->toJson();
        }

        return view('product.index');
    }

    public function create()
    {
        return view('product.create');
    }

    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'user_id' => 'required',
                'user_name' => 'required|string',
                'file' => 'required|file|mimetypes:text/csv'
            ]);
    
            $client = new Client(['base_uri' => env('SERVICE_HOST')]);
    
            $options = [
                'Accept' => 'application/json', 
            ];
    
            try {
                $response = $client->request(
                    'POST',
                    '/product',
                    [
                        'headers' => $options,
                        'multipart' => [
                            [
                                'name' => 'user_id',
                                'contents' => $request->input('user_id')
                            ],
                            [
                                'name' => 'user_name',
                                'contents' => $request->input('user_name')
                            ],
                            [
                                'name' => 'file',
                                'contents' => fopen($request->file('file'), 'r')
                            ]
                        ]
                    ]
                );

                return $response->getBody();
            } catch (RequestException $requestException) {
                return $requestException->getResponse();
            }
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => 'Failed to insert data',
                'data' => $th->getMessage()
            ], 500);
        }
    }
}
