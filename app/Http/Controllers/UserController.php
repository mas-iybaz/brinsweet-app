<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class UserController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        $client = new Client(['base_uri' => env('SERVICE_HOST')]);

        $options = [
            'Accept' => 'application/json', 
        ];

        $req = $client->request(
            'GET',
            '/user',
            $options
        );
        $res = $req->getBody();

        $users = json_decode($res);
        $users = $users->data;
        // dd($users);

        if ($request->ajax()) {
            return datatables()->of($users)
            ->addIndexColumn()
            ->toJson();
        }

        return view('user.index', compact(['users']));
    }
}
